package info.mallmc.core;

import info.mallmc.core.api.Server;
import info.mallmc.core.api.language.Language;
import info.mallmc.core.api.player.EnumLanguageIdentifier;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.database.Database;
import info.mallmc.core.database.Redis;
import info.mallmc.core.database.ServerData;
import info.mallmc.core.util.MallCoreSchedulerService;
import info.mallmc.core.util.XPManager;

import java.util.Map;

public class MallCore {

  private static MallCore instance;

  public static MallCore getInstance() {
    return instance;
  }

  private Database database;
  private Redis redis;
  private Map<EnumLanguageIdentifier, Language> languageMap;
  private String serverIP;
  private int serverPort;
  private Server server;
  private XPManager xpManager;
  private MallCoreSchedulerService mallCoreSchedulerService;

  public MallCore(String serverIP, int serverPort) {
    instance = this;
    database = new Database();
    redis = new Redis();
    xpManager = new XPManager(1);
    this.serverIP = serverIP;
    this.serverPort = serverPort;
    this.mallCoreSchedulerService = new MallCoreSchedulerService();
  }

  public MallCore() {
    instance = this;
    database = new Database();
    redis = new Redis();
  }

  public void connect(String dbIP, int dbPort, String dbUsername, String dbPassword){
    database.connect(dbIP, dbPort, dbUsername, dbPassword);
  }

  public void redisConnect(String host, String password, int port){
    redis.connect(host, password, port);
  }

  public Server getServer(){
    if(server == null){
      server = ServerData.getInstance().getCurrentServer(serverIP, serverPort);
      if(server == null){
        server = new Server(serverIP, serverPort);
        ServerData.getInstance().saveServer(server);
      }
    }
    return server;
  }

  public Database getDatabase() {
    return database;
  }

  public Redis getRedis() {
    return redis;
  }

  public Map<EnumLanguageIdentifier, Language> getLanguageMap() {
    return languageMap;
  }

  public void setLanguageMap(Map<EnumLanguageIdentifier, Language> languageMap) {
    this.languageMap = languageMap;
  }

  public String getServerIP() {
    return serverIP;
  }

  public MallCoreSchedulerService getMallCoreSchedulerService() {
    return mallCoreSchedulerService;
  }

  public int getServerPort() {
    return serverPort;
  }

  public void disable() {
    MallPlayer.getPlayers().clear();
    database.close();
    if(redis != null) {
      redis.stop();
    }
  }
}
