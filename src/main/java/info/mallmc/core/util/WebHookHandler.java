package info.mallmc.core.util;

import info.mallmc.core.api.logs.LL;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class WebHookHandler {

  public static void sendDiscordAlert(LL level, String message, String trigger, String server){
    JSONObject data = new JSONObject();
    data.put("embeds", getEmbeds(level, message, trigger,server));
    try {
      HttpURLConnection connection = (HttpURLConnection) new URL("https://canary.discordapp.com/api/webhooks/371238240159662081/Sp_Z2rjJ8Bht98f4O6w76btgJ247YAlW-8VCa_C032sAIDrNMoLxB6JF74o5OIxbcbVG").openConnection();
      connection.setRequestMethod("POST");
      connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
      connection.setConnectTimeout(5000);
      connection.setUseCaches(false);
      connection.setDoInput(true);
      connection.setDoOutput(true);
      OutputStream os = connection.getOutputStream();
      os.write(data.toJSONString().getBytes("UTF-8"));
      os.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static JSONArray getEmbeds(LL level, String message, String trigger, String server){
    JSONArray embeds = new JSONArray();
    JSONObject embed = new JSONObject();
    embed.put("title", "Alert");
    embed.put("description", "An error has occured, please read for more details");
    embed.put("color", "0xff3338");
    JSONArray fieldsArray = new JSONArray();
    JSONObject levelOBJ = new JSONObject();
    levelOBJ.put("name", "Level");
    levelOBJ.put("value", level);
    levelOBJ.put("inline", false);
    JSONObject messageOBJ = new JSONObject();
    messageOBJ.put("name", "Message");
    messageOBJ.put("value", message);
    messageOBJ.put("inline", false);
    JSONObject triggerOBJ = new JSONObject();
    triggerOBJ.put("name", "Trigger");
    triggerOBJ.put("value", trigger);
    triggerOBJ.put("inline", false);
    JSONObject serverOBJ = new JSONObject();
    serverOBJ.put("name", "Server");
    serverOBJ.put("value", server);
    serverOBJ.put("inline", false);
    fieldsArray.addAll(Arrays.asList(levelOBJ, messageOBJ, triggerOBJ, serverOBJ));
    embed.put("fields", fieldsArray);
    embeds.add(embed);
    return embeds;
  }

}
