package info.mallmc.core.util;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;

import java.util.ArrayList;
import java.util.HashMap;

public class XPManager {

  private static XPManager xpManager;

  private int maxLevel;
  private int globalModifer;


  public XPManager(int globalModifer) {
    this.maxLevel = 100;
    this.globalModifer = globalModifer;
    xpManager = this;
  }


  public float getNeededXPForNextLevel(MallPlayer player) {
    float level = player.getLevel();
    if (player.getLevel() == maxLevel) {
      return -1;
    } else {
      return getXPForLevel(level + 1.0F) - player.getXp();
    }
  }


  public void addXP(MallPlayer mallPlayer, float amount) {
    mallPlayer.addXP(amount * globalModifer * MallCore.getInstance().getServer().getXpModifer());
  }

  public void setLevel(MallPlayer player, int level) {
      player.setXp(level);
  }

  public float getLevelFormXP(float xp)
  {
    int baseModifer;
    int baseAdding;
    if(xp <= 315 ) {
      baseModifer = 2;
      baseAdding = 7;
    }else if (xp <= 1395) {
      baseModifer = 5;
      baseAdding = -38;
    } else {
      baseModifer = 9;
      baseAdding = -158;
    }

    return (xp - baseAdding) / baseModifer;
  }

  public float getXPForLevel(float level)
  {
    if(level > maxLevel) {
      return -1;
    }
    int baseModifer;
    int baseAdding;

    if(level <= 15 ) {
      baseModifer = 2;
      baseAdding = 7;
    }else if (level <= 30) {
      baseModifer = 5;
      baseAdding = -38;
    } else {
      baseModifer = 9;
      baseAdding = -158;
    }
    return baseModifer * (level - 1) + baseAdding;
  }

  public static XPManager getXpManager() {
    return xpManager;
  }
}