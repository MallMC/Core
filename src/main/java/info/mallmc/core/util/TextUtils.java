package info.mallmc.core.util;

import net.md_5.bungee.api.ChatColor;

public class TextUtils {

  /**
   * Replace sections in string with objects
   *
   * @param msg The message to have replaced with objects
   * @param replacements The replacements to replace variables
   * @return The replaced message.
   */
  public static String replace(String msg, Object... replacements) {
    if (replacements.length > 0) {
      for (int i = 0; i < replacements.length; i++) {
        if (msg.contains("{" + i + "}")) {
          msg = msg.replace("{" + i + "}", replacements[i].toString());
        } else {
          msg = msg.replace("{" + i + "}", "");
        }
      }
    }
    return colorizeMessageOld(msg);
  }

  /**
   * Colorize a message using the old ChatColor System
   *
   * @param message The message to colorize
   * @return The colorized message in string form.
   */
  public static String colorizeMessageOld(String message) {
    return ChatColor.translateAlternateColorCodes('&', message);
  }

  private final static int CENTER_PX = 154;

  public static String getCenteredMessage(String message) {

    message = ChatColor.translateAlternateColorCodes('&', message);

    int messagePxSize = 0;
    boolean previousCode = false;
    boolean isBold = false;

    for (char c : message.toCharArray()) {
      if (c == '�') {
        previousCode = true;
        continue;
      } else if (previousCode == true) {
        previousCode = false;
        if (c == 'l' || c == 'L') {
          isBold = true;
          continue;
        } else {
          isBold = false;
        }
      } else {
        DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
        messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
        messagePxSize++;
      }
    }

    int halvedMessageSize = messagePxSize / 2;
    int toCompensate = CENTER_PX - halvedMessageSize;
    int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
    int compensated = 0;
    StringBuilder sb = new StringBuilder();
    while (compensated < toCompensate) {
      sb.append(" ");
      compensated += spaceLength;
    }
    return sb.toString() + message;
  }

}
