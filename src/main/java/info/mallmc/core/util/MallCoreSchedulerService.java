package info.mallmc.core.util;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import info.mallmc.core.MallCore;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MallCoreSchedulerService {

  private final ExecutorService executorService;

  public MallCoreSchedulerService() {
    this.executorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors(), new ThreadFactoryBuilder().setNameFormat("scheduler-%d").setUncaughtExceptionHandler(new SchedulerExceptionHandler()).build());
    Runtime.getRuntime().addShutdownHook(new Thread(executorService::shutdown));
  }

  public void execute(Runnable task){
    executorService.execute(task);
  }

  public void shutdown(){
    executorService.shutdown();
  }

  public void forceShutdown(){
    executorService.shutdownNow();
  }

  class SchedulerExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable ex) {
      Log.error(LL.CRITICAL, MallCore.getInstance().getServer().getNickname(), "Thread","Uncaught exception in thread " + t.getName(), ex.getMessage());
    }
  }
}
