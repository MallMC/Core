package info.mallmc.core.util.discord;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiscordMessage
{
  private String title;
  private String description;
  private Color color;
  private String authorIconUrl;
  private List<DiscordMessageField> fields = new ArrayList();

  public DiscordMessage(String title) {
    this.title = title;
  }

  public DiscordMessage setTitle(String title) {
    this.title = title;
    return this;
  }

  public DiscordMessage setDescription(String description) {
    this.description = description;
    return this;
  }

  public DiscordMessage setColor(Color color) {
    this.color = color;
    return this;
  }

  public void setAuthorIconUrl(String authorIconUrl) {
    this.authorIconUrl = authorIconUrl;
  }

  public void addField(DiscordMessageField... discordMessageFields) {
    Collections.addAll(fields, discordMessageFields);
  }

  public Color getColor() {
    return color;
  }

  public List<DiscordMessageField> getFields() {
    return fields;
  }

  public String getAuthorIconUrl() {
    return authorIconUrl;
  }

  public String getDescription() {
    return description;
  }

  public String getTitle() {
    return title;
  }
}
