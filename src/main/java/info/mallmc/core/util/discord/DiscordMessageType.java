package info.mallmc.core.util.discord;

public enum  DiscordMessageType
{
    ALERT,
    CHAT,
    LINK,
}
