package info.mallmc.core.util.discord;

public class DiscordMessageField
{
  private String name;
  private String text;
  private boolean inLine;

  public DiscordMessageField(String name, String text, boolean inLine)
  {
    this.name = name;
    this.text = text;
    this.inLine = inLine;
  }

  public String getName() {
    return name;
  }

  public String getText() {
    return text;
  }

  public boolean isInLine() {
    return inLine;
  }
}
