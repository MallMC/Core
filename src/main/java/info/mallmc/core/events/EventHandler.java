package info.mallmc.core.events;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.database.PlayerData;
import info.mallmc.core.events.events.PlayerChangeEvent;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public class EventHandler {

  private static Multimap<EventType, Consumer<Event>> events = ArrayListMultimap.create();

  public static void registerEvent(EventType event, Consumer<Event> eventConsumer) {
    events.put(event, eventConsumer);
  }

  public static void handleEvent(EventType type, Event event) {
    List<Consumer<Event>> interactionsToHandle = (List<Consumer<Event>>) events.get(type);
    for (Consumer<Event> runnable : interactionsToHandle) {
      runnable.accept(event);
    }
    if (type.isPlayerChange() && type != EventType.PLAYER_CHANGE) {
      List<Consumer<Event>> interactionsToHandlePlayer = (List<Consumer<Event>>) events
          .get(EventType.PLAYER_CHANGE);
      for (Consumer<Event> runnable : interactionsToHandlePlayer) {
        runnable.accept(event);
      }
    }
  }

  public static void triggerEvent(EventType type, Event event) {
    System.out.println("" + type.getChannel() + " event " + event);
    if (event.isRedisEvent()) {
      event.sendInfoToRedis(type);
    } else {
      handleEvent(type, event);
    }


  }


}
