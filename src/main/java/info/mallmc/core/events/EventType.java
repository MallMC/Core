package info.mallmc.core.events;

public enum EventType {

  PLAYER_CURRENCY_CHANGE("playerUpdateCurrency", true),
  PLAYER_RANK_CHANGE("playerUpdateRank", true),
  PLAUER_LEVEUP_EVENT("playerLevelupEvent", true),
  PLAYER_CHANGE("playerUpdate", true),
  PLAYER_XP_CHANGE("playerUpdateXp", true),
  GAMESTATE_CHANGE_EVENT("gameStateUpdate", false),
  JOIN_GAME_EVENT("joinGameEvent", false),
  ADD_SERVER_EVENT("addServerEvent", false),
  GAME_END_EVENT("gameEndEvent", false),
  SERVER_READY_EVENT("serverReadyEvent", false),
  GAME_START_EVENT("gameStartEvent", false),
  REMOVE_SERVER_EVENT("removeServerEvent", false),
  LANG_RELOAD("langReload", false),
  PLAYER_READY("playerReady", false),
  MESSAGE_TO_DISCORD_EVENT("toDiscord", false),
  MESSAGE_FORM_DISCORD_EVENT("formDiscord", false);

  private String channel;
  private boolean isPlayerChange;

  EventType(String channel, boolean isPlayerChange) {
    this.channel = channel;
    this.isPlayerChange = isPlayerChange;
  }

  public boolean isPlayerChange() {
    return isPlayerChange;
  }

  public String getChannel() {
    return channel;
  }

  public static EventType getTypeFormString(String name) {
    for (EventType type : values()) {
      if (type.channel.equalsIgnoreCase(name))
      {
        return type;
      }
    }
    return null;
  }
}
