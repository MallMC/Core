package info.mallmc.core.events.events.discord;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.Rank;
import info.mallmc.core.events.Event;
import info.mallmc.core.events.EventType;

public class DiscordMessageReceivedEvent extends Event
{
  private final Rank rank;
  private final String username;
  private final String message;

  public DiscordMessageReceivedEvent(String message, String username, Rank rank)
  {
    this.message = message;
    this.username = username;
    this.rank = rank;
  }

  public String getMessage() {
    return message;
  }

  public Rank getRank() {
    return rank;
  }

  public String getUsername() {
    return username;
  }

  @Override
  public boolean isRedisEvent() {
    return true;
  }

  @Override
  public void sendInfoToRedis(EventType eventType) {
    MallCore.getInstance().getRedis().sendMessage(eventType.getChannel(), MallCore.getInstance().getRedis().getGson().toJson(this));
  }
}
