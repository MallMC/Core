package info.mallmc.core.events.events;

import info.mallmc.core.events.Event;

public class RemoveServerEvent extends Event {

  private String serverUUID;

  public String getServerUUID() {
    return serverUUID;
  }

  public RemoveServerEvent(String serverUUID) {
    this.serverUUID = serverUUID;
  }
}