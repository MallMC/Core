package info.mallmc.core.events.events;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.events.Event;

public class JoinGameEvent extends Event {

  private MallPlayer mallPlayer;
  private String gameName;

  public MallPlayer getMallPlayer() {
    return mallPlayer;
  }

  public String getGameName() {
    return gameName;
  }

  public JoinGameEvent(MallPlayer updatedPlayer, String gameName) {
    this.mallPlayer = updatedPlayer;
    this.gameName = gameName;
  }

}
