package info.mallmc.core.events.events;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.events.Event;

public class PlayerReadyEvent extends Event {

  private MallPlayer player;

  public PlayerReadyEvent(MallPlayer player) {
    this.player = player;
  }

  public MallPlayer getPlayer() {
    return player;
  }
}
