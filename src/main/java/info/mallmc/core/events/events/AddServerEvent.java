package info.mallmc.core.events.events;

import info.mallmc.core.events.Event;

public class AddServerEvent extends Event {

  private String serverIP;
  private int serverPort;

  public String getServerIP() {
    return serverIP;
  }

  public int getServerPort() {
    return serverPort;
  }

  public AddServerEvent(String serverIP, int serverPort) {
    this.serverIP = serverIP;
    this.serverPort = serverPort;
  }
}