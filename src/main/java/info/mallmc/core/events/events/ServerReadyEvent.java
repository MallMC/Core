package info.mallmc.core.events.events;

import info.mallmc.core.api.Server;
import info.mallmc.core.events.Event;

public class ServerReadyEvent extends Event {

  private Server server;
  private String gameName;

  public Server getServer() {
    return server;
  }

  public String getGameName() {
    return gameName;
  }

  public ServerReadyEvent(Server server, String gameName) {
    this.server = server;
    this.gameName = gameName;
  }


}
