package info.mallmc.core.events.events.discord;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.events.Event;
import info.mallmc.core.events.EventType;
import info.mallmc.core.util.discord.DiscordMessage;
import info.mallmc.core.util.discord.DiscordMessageType;

public class SendMessageToDiscordEvent extends Event
{
    private final DiscordMessage discordMessage;
    private final DiscordMessageType type;
    private final MallPlayer mallPlayer;

    public SendMessageToDiscordEvent(DiscordMessage discordMessage, DiscordMessageType type, MallPlayer mallPlayer)
    {
        this.discordMessage = discordMessage;
        this.type = type;
        this.mallPlayer = mallPlayer;
    }

    public MallPlayer getMallPlayer() {
        return mallPlayer;
    }

    public DiscordMessage getDiscordMessage() {
        return discordMessage;
    }

    public DiscordMessageType getType() {
        return type;
    }

    @Override
    public boolean isRedisEvent() {
        return true;
    }

    @Override
    public void sendInfoToRedis(EventType eventType) {
        MallCore.getInstance().getRedis().sendMessage(eventType.getChannel(), MallCore.getInstance().getRedis().getGson().toJson(this));
    }
}
