package info.mallmc.core.events.events;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.events.Event;
import info.mallmc.core.events.EventType;

public class PlayerChangeEvent extends Event {

  private MallPlayer mallPlayer;

  public MallPlayer getUpdatedPlayer() {
    return mallPlayer;
  }

  public PlayerChangeEvent(MallPlayer updatedPlayer) {
    this.mallPlayer = updatedPlayer;
  }

  @Override
  public boolean isRedisEvent() {
    return true;
  }

  @Override
  public void sendInfoToRedis(EventType eventType) {
    MallCore.getInstance().getRedis().sendMessage(eventType.getChannel(),  MallCore.getInstance().getRedis().getGson().toJson(this));
  }
}
