package info.mallmc.core.events.events;

import info.mallmc.core.api.Server;
import info.mallmc.core.events.Event;

public class GameEndEvent extends Event {

  private Server server;

  public Server getServer() {
    return server;
  }

  public GameEndEvent(Server server) {
    this.server = server;
  }

}
