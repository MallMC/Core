package info.mallmc.core.api.bans;

import java.util.UUID;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("bans")
public class Ban {

  @Id
  private ObjectId id;
  private UUID banner;
  private UUID banned;
  private int points;
  private String reason;
  private String evidence;
  private String startDate;
  private String endDate;

  public Ban(UUID banner, UUID banned, int points, String reason, String evidence,
      String startDate, String endDate) {
    this.banner = banner;
    this.banned = banned;
    this.points = points;
    this.reason = reason;
    this.evidence = evidence;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public UUID getBanner() {
    return banner;
  }

  public void setBanner(UUID banner) {
    this.banner = banner;
  }

  public UUID getBanned() {
    return banned;
  }

  public void setBanned(UUID banned) {
    this.banned = banned;
  }

  public int getPoints() {
    return points;
  }

  public void setPoints(int points) {
    this.points = points;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getEvidence() {
    return evidence;
  }

  public void setEvidence(String evidence) {
    this.evidence = evidence;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }
}
