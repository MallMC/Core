package info.mallmc.core.api.bans;

import info.mallmc.core.api.player.PermissionSet;
import java.util.Arrays;
import java.util.List;

public enum Offense {

  NONE(PermissionSet.ADMIN, "Nothing", 0, Arrays.asList("nothing")),
  BAN_EVASION(PermissionSet.STAFF, "Ban Evasion", 10, Arrays.asList("evasion", "banevasion")),
  SPAM(PermissionSet.STAFF, "Spam", 3, Arrays.asList("spam", "spamming"));

  private PermissionSet perms;

  private String name;
  private int points;
  private List<String> aliases;

  Offense(PermissionSet perms, String name, int points, List<String> aliases) {
    this.perms = perms;
    this.name = name;
    this.points = points;
    this.aliases = aliases;
  }

  /**
   * Get the offense by its name or alias
   *
   * @param offense The name or alias to find.
   * @return The offense by that name or alias.
   */
  public static Offense getOffense(String offense) {
    for (Offense p : Offense.values()) {
      if (p.getName().equalsIgnoreCase(offense)) {
        return p;
      }
    }
    for (Offense o : Offense.values()) {
      for (String oName : o.getAliases()) {
        if (oName.equalsIgnoreCase(offense)) {
          return o;
        }
      }
    }
    return null;
  }

  /**
   * Get the minimum permissions required to use that offense
   *
   * @return The permissionset required to use that offense
   * @see PermissionSet
   */
  public PermissionSet getMinimumPermissions() {
    return perms;
  }

  /**
   * Get the name of the offense
   *
   * @return The name of the offense
   */
  public String getName() {
    return name;
  }

  /**
   * Get the points that this offense carries
   *
   * @return The amount of points that this offense carries.
   */
  public int getPoints() {
    return points;
  }

  /**
   * Get the aliases of this offense
   *
   * @return The aliases of this offense
   */
  public List<String> getAliases() {
    return aliases;
  }
}
