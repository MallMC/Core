package info.mallmc.core.api;

import info.mallmc.core.api.player.PermissionSet;

public enum ChatChannel {

  ALL(PermissionSet.DEFAULT, "Public"),
  STAFF(PermissionSet.SPECIAL, "Staff"),
  PARTY(PermissionSet.DEFAULT, "Party");

  private PermissionSet permissionSet;
  private String channelName;

  ChatChannel(PermissionSet permissionSet, String channelName) {
    this.permissionSet = permissionSet;
    this.channelName = channelName;
  }

  public PermissionSet getPermissionSet() {
    return permissionSet;
  }

  public String getChannelName() {
    return channelName;
  }
}
