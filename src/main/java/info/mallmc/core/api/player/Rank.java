package info.mallmc.core.api.player;

import net.md_5.bungee.api.ChatColor;

public enum Rank {

  DEFAULT("Visitor", 0, PermissionSet.DEFAULT, ChatColor.GRAY),
  CUSTOMER("Customer", 5, PermissionSet.PREMIUM, ChatColor.LIGHT_PURPLE),
  LOYALTY("Loyalty", 6, PermissionSet.PREMIUM_PLUS, ChatColor.GOLD),
  VIP("VIP", 7, PermissionSet.VIP, ChatColor.DARK_BLUE),
  MEDIA("Media", 15, PermissionSet.MEDIA, ChatColor.DARK_PURPLE),
  CONTRACTOR("Contractor", 20, PermissionSet.SPECIAL, ChatColor.BLUE),
  STAFF("Staff", 25, PermissionSet.STAFF, ChatColor.YELLOW),
  MOD("Mod", 30, PermissionSet.MOD, ChatColor.GREEN),
  ADMIN("Admin", 35, PermissionSet.ADMIN, ChatColor.AQUA);

  private String name;
  private int id;
  private PermissionSet permissions;
  private ChatColor color;

  Rank(String name, int id, PermissionSet permissions, ChatColor color) {
    this.name = name;
    this.id = id;
    this.permissions = permissions;
    this.color = color;
  }

  public String getName() {
    return name;
  }

  public int getId()
  {
    return id;
  }

  public PermissionSet getPermissions() {
    return permissions;
  }

  public ChatColor getColor() {
    return color;
  }

  public String getPrefix() {
    if (this == Rank.DEFAULT) {
      return color.toString();
    } else {
      return ChatColor.BOLD.toString() +  ChatColor.WHITE + "[" + color.toString() + name + ChatColor.WHITE + "] ";
    }
  }

  public static Rank getRankFromID(int power) {
    Rank rank = Rank.DEFAULT;
    for (Rank rank1 : Rank.values()) {
      if (rank1.getId() == power) {
        rank = rank1;
      }
    }
    return rank;
  }

  public static Rank getRankFromName(String name, Rank error) {
    Rank rank = error;
    for (Rank rank1 : Rank.values()) {
      if (rank1.name().equalsIgnoreCase(name)) {
        rank = rank1;
      }
    }
    return rank;
  }

}
