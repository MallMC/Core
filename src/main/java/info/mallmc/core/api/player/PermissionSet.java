package info.mallmc.core.api.player;

public enum PermissionSet {

  DEFAULT(0),
  PREMIUM(5),
  PREMIUM_PLUS(6),
  VIP(7),
  MEDIA(20),
  SPECIAL(25),
  STAFF(25),
  MOD(30),
  ADMIN(35);

  private int power;

  PermissionSet(int power) {
    this.power = power;
  }

  public int getPower() {
    return power;
  }

  public static PermissionSet getPermissionSetFromPower(int power) {
    PermissionSet permissionSet = PermissionSet.DEFAULT;
    for (PermissionSet permSet : PermissionSet.values()) {
      if (permSet.getPower() == power) {
        permissionSet = permSet;
      }
    }
    return permissionSet;
  }
}













