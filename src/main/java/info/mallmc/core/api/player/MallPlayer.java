package info.mallmc.core.api.player;

import info.mallmc.core.api.ChatChannel;
import info.mallmc.core.api.GameState;
import info.mallmc.core.database.PlayerData;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;

import info.mallmc.core.events.events.PlayerChangeEvent;
import info.mallmc.core.events.events.PlayerReadyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import info.mallmc.core.util.XPManager;
import java.util.concurrent.CompletableFuture;
import org.bson.types.ObjectId;
import org.bukkit.ChatColor;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.PostLoad;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

@Entity("players")
public class MallPlayer {

  private static HashMap<UUID, MallPlayer> players = new HashMap<>();

  public static HashMap<UUID, MallPlayer> getPlayers() {
    return players;
  }

  public static boolean isOnline(UUID uuid){
    return players.containsKey(uuid);
  }

  /**
   * Get a player from the current list of players.
   *
   * @param uuid The uuid of the player to get
   * @return The player from the list
   */
  public static MallPlayer getPlayer(UUID uuid) {
    if (!players.containsKey(uuid)) {
      return null;
    }
    return players.get(uuid);
  }

  /**
   * Add a player to the list or create a player
   *
   * @param uuid The UUID of the player to create / add
   * @param username The username of the player to create / add
   * @return The player that was created / added
   */
  public static CompletableFuture<MallPlayer> createPlayer(UUID uuid, String username) {
    CompletableFuture<MallPlayer> completableFuture = new CompletableFuture<>();
    if (!players.containsKey(uuid)) {
      PlayerData.getInstance().isInDatabase(uuid).whenComplete((isInDatabase, throwable) -> {
        if(isInDatabase){
          PlayerData.getInstance().getPlayerByUUID(uuid).whenComplete(((mallPlayer, throwable1) -> {
            players.put(uuid, mallPlayer);
            completableFuture.complete(mallPlayer);
          }));
        }else{
          MallPlayer mallPlayer = new MallPlayer(uuid, username);
          players.put(uuid, mallPlayer);
          completableFuture.complete(mallPlayer);
          EventHandler.handleEvent(EventType.PLAYER_READY, new PlayerReadyEvent(mallPlayer));
        }
      });
    } else {
      completableFuture.complete(players.get(uuid));
    }
    return completableFuture;
  }

  public static void removePlayer(UUID uuid) {
    if (players.containsKey(uuid)) {
      players.remove(uuid);
    }
  }

  @Id
  private ObjectId id;
  @Indexed(options = @IndexOptions(unique = true))
  private UUID uuid;
  private String username;
  @Transient
  private Rank rank;
  @Transient
  private PermissionSet permissionSet;
  @Property("rank")
  private int rankID;
  @Property("permissions")
  private int permissionPower;
  private int currency;
  private float xp;
  private String lastBan;
  private String lastServer;
  private long loginTime;
  private long playTime;
  private String lastIP;
  private boolean banned;
  private int banPoints;
  private String banExpiry;
  private String banReason;
  private EnumLanguageIdentifier languageIdentifier;
  private String discordID;
  @Transient
  private int level;
  private long lastLogin;
  @Transient
  private boolean frozen;
  @Transient
  private ArrayList<String> recentMessages;
  @Transient
  private boolean chatMuted;
  @Transient
  private boolean inSpecChat;
  private List<UUID> friends;
  private boolean friendRequestsEnabled;
  private List<UUID> friendRequests;
  @Transient
  private ChatChannel chatChannel;
  @Transient
  private boolean debugMode;
  @Transient
  private String chatPrefix;
  @Transient
  private String chatSuffix;

  public MallPlayer() {
    this.frozen = false;
    this.recentMessages = new ArrayList<>();
    this.chatMuted = false;
    this.inSpecChat = false;
    this.rank = Rank.getRankFromID(this.rankID);
    this.permissionSet = PermissionSet.getPermissionSetFromPower(this.permissionPower);
    this.chatChannel = ChatChannel.ALL;
    this.debugMode  = false;
    this.chatPrefix = null;
    this.chatSuffix = null;
  }

  private MallPlayer(UUID uuid, String username) {
    this.uuid = uuid;
    this.username = username;
    this.lastLogin = System.currentTimeMillis();
    this.rank = Rank.DEFAULT;
    this.permissionSet = PermissionSet.DEFAULT;
    this.rankID = 0;
    this.permissionPower = 0;
    this.frozen = false;
    this.chatMuted = false;
    this.inSpecChat = false;
    this.recentMessages = new ArrayList<>();
    this.currency = 0;
    this.friends = new ArrayList<>();
    this.friendRequestsEnabled = true;
    this.friendRequests = new ArrayList<>();
    this.chatChannel = ChatChannel.ALL;
    this.lastBan ="";
    this.lastServer = "hub1";
    this.languageIdentifier = EnumLanguageIdentifier.EN;
    this.playTime = 0;
    this.lastIP = "";
    this.banned = false;
    this.banPoints = 0;
    this.debugMode = false;
    this.chatPrefix = null;
    this.chatSuffix = null;
  }
  @PrePersist
  void prePersist() {
    this.rankID = rank.getId();
    this.permissionPower = permissionSet.getPower();
  }

  @PostLoad
  void postLoad(){
    this.rank = Rank.getRankFromID(this.rankID);
    this.permissionSet = PermissionSet.getPermissionSetFromPower(this.permissionPower);
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public UUID getUuid() {
    return uuid;
  }

  public String getUsername() {
    return username;
  }

  public Rank getRank() {
    return rank;
  }

  public void setRank(Rank rank) {
    this.rank = rank;
    this.rankID = rank.getId();
    EventHandler.triggerEvent(EventType.PLAYER_RANK_CHANGE, new PlayerChangeEvent(this));
  }

  public PermissionSet getPermissionSet() {
    return permissionSet;
  }

  public void setPermissionSet(PermissionSet permissionSet) {
    this.permissionSet = permissionSet;
    this.permissionPower = permissionSet.getPower();
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public int getCurrency() {
    return currency;
  }

  public void setCurrency(int currency) {
    this.currency = currency;
    EventHandler.triggerEvent(EventType.PLAYER_CURRENCY_CHANGE, new PlayerChangeEvent(this));
  }

  public boolean purchase(int price) {
    if(this.currency == -1) {
      return true;
    }
    if (this.currency > price) {
      this.currency -= price;
      EventHandler.triggerEvent(EventType.PLAYER_CURRENCY_CHANGE, new PlayerChangeEvent(this));
      return true;
    } else {
      return false;
    }
  }

  public boolean addMoney(int amount) {
    if(this.currency == -1) {
      return true;
    }
    if (amount > 0) {
      this.currency += amount;
      EventHandler.triggerEvent(EventType.PLAYER_CURRENCY_CHANGE, new PlayerChangeEvent(this));
      return true;
    } else {
      return false;
    }
  }

  public float getXp() {
    return xp;
  }

  public void setXp(float xp) {
    this.xp = xp;
    EventHandler.triggerEvent(EventType.PLAYER_XP_CHANGE, new PlayerChangeEvent(this));
  }
  public void addXP(float xpL)
  {
    if(XPManager.getXpManager().getLevelFormXP(xpL) > XPManager.getXpManager().getLevelFormXP(this.xp)) {
      EventHandler.triggerEvent(EventType.PLAUER_LEVEUP_EVENT, new PlayerChangeEvent(this));
    }
    setXp(xpL);
  }

  public float getLevel() {
    return XPManager.getXpManager().getLevelFormXP(this.xp);
  }

  public boolean isFrozen() {
    return frozen;
  }

  public void setFrozen(boolean frozen) {
    this.frozen = frozen;
  }

  public ArrayList<String> getRecentMessages() {
    return recentMessages;
  }

  public void addRecentMessage(String message) {
    recentMessages.add(message);
    if (recentMessages.size() > 6) {
      recentMessages.remove(0);
    }
  }


  public boolean isChatMuted() {
    return chatMuted;
  }

  public void setChatMuted(boolean chatMuted) {
    this.chatMuted = chatMuted;
  }

  public boolean isInSpecChat() {
    return inSpecChat;
  }

  public void setInSpecChat(boolean inSpecChat) {
    this.inSpecChat = inSpecChat;
  }

  public String getLastBan() {
    return lastBan;
  }

  public void setLastBan(String lastBan) {
    this.lastBan = lastBan;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public String getLastServer() {
    return lastServer;
  }

  public void setLastServer(String lastServer) {
    this.lastServer = lastServer;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public long getLoginTime() {
    return loginTime;
  }

  public void setLoginTime(long loginTime) {
    this.loginTime = loginTime;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public long getPlayTime() {
    return playTime;
  }

  public void setPlayTime(long playTime) {
    this.playTime = playTime;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public String getLastIP() {
    return lastIP;
  }

  public void setLastIP(String lastIP) {
    this.lastIP = lastIP;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public boolean isBanned() {
    return banned;
  }

  public void setBanned(boolean banned) {
    this.banned = banned;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public int getBanPoints() {
    return banPoints;
  }

  public void setBanPoints(int banPoints) {
    this.banPoints = banPoints;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public String getBanExpiry() {
    return banExpiry;
  }

  public void setBanExpiry(String banExpiry) {
    this.banExpiry = banExpiry;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  public String getBanReason() {
    return banReason;
  }

  public void setBanReason(String banReason) {
    this.banReason = banReason;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  /**
   * Get if friend requests are enabled for this player
   *
   * @return If friend requests are enabled
   */
  public boolean isFriendRequestsEnabled() {
    return friendRequestsEnabled;
  }

  /**
   * Set friend requests to enabled
   *
   * @param friendRequestsEnabled what to set the friends requests enabled to
   */
  public void setFriendRequestsEnabled(boolean friendRequestsEnabled) {
    this.friendRequestsEnabled = friendRequestsEnabled;
    EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
  }

  /**
   * Get the players friend requests
   *
   * @return The players friend requests
   */
  public List<UUID> getFriendRequests() {
    return friendRequests;
  }

  /**
   * Get the players friends
   *
   * @return The players friends
   */
  public List<UUID> getFriends() {
    return friends;
  }

  public void setFriends(List<UUID> friends) {
    this.friends = friends;
  }

  public void setFriendRequests(List<UUID> friendRequests) {
    this.friendRequests = friendRequests;
  }

  public CompletableFuture<FriendResult> addFriend(UUID friendUUID) {
    CompletableFuture<FriendResult> friendResultCompletableFuture = new CompletableFuture<>();
    if (friendUUID == this.uuid) {
      friendResultCompletableFuture.complete(FriendResult.FRIEND_SELF);
    }
    UUID uuid = friendUUID;
    if (getPlayer(uuid) == null) {
      PlayerData.getInstance().getPlayerByUUID(uuid).whenComplete(((mp, throwable) -> {
        if (mp == null) {
          friendResultCompletableFuture.complete(FriendResult.PLAYER_DOES_NOT_EXIST);
        }
        if (!mp.isFriendRequestsEnabled()) {
          friendResultCompletableFuture.complete(FriendResult.FRIENDS_DISABLED);
        }
        if(mp.getFriends() == null){
          mp.setFriends(new ArrayList<>());
        }
        if(mp.getFriendRequests() == null){
          mp.setFriendRequests(new ArrayList<>());
        }
        if (mp.getFriends().contains(friendUUID)) {
          friendResultCompletableFuture.complete(FriendResult.ALREADY_FRIENDS);
        }
        if (this.getFriends().contains(friendUUID)) {
          friendResultCompletableFuture.complete(FriendResult.ALREADY_FRIENDS);
        }
        if (mp.getFriendRequests().contains(this.uuid)) {
          friendResultCompletableFuture.complete(FriendResult.ALREADY_FRIEND_REQUEST);
        }
        mp.getFriendRequests().add(this.uuid);
        EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
      }));
    } else {
      MallPlayer mp = MallPlayer.getPlayer(uuid);
      if (mp == null) {
        friendResultCompletableFuture.complete(FriendResult.PLAYER_DOES_NOT_EXIST);
      }
      if (!mp.isFriendRequestsEnabled()) {
        friendResultCompletableFuture.complete(FriendResult.FRIENDS_DISABLED);
      }
      if(mp.getFriends() == null){
        mp.setFriends(new ArrayList<>());
      }
      if(mp.getFriendRequests() == null){
        mp.setFriendRequests(new ArrayList<>());
      }
      if (mp.getFriends().contains(friendUUID)) {
        friendResultCompletableFuture.complete(FriendResult.ALREADY_FRIENDS);
      }
      if (this.getFriends().contains(friendUUID)) {
        friendResultCompletableFuture.complete(FriendResult.ALREADY_FRIENDS);
      }
      if (mp.getFriendRequests().contains(this.uuid)) {
        friendResultCompletableFuture.complete(FriendResult.ALREADY_FRIEND_REQUEST);
      }
      mp.getFriendRequests().add(this.uuid);
      EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
    }
    return friendResultCompletableFuture;
  }

  public CompletableFuture<FriendResult> acceptFriendRequest(UUID requestUUID) {
    CompletableFuture<FriendResult> completedFuture = new CompletableFuture<>();
    if (requestUUID == this.uuid) {
      completedFuture.complete(FriendResult.FRIEND_SELF);
    }
    if (!getFriendRequests().contains(requestUUID)) {
      completedFuture.complete(FriendResult.NO_FRIEND_REQUEST);
    }
    if (getPlayer(requestUUID) == null) {
      PlayerData.getInstance().getPlayerByUUID(requestUUID).whenComplete((mp, throwable) -> {
        if (mp == null) {
          completedFuture.complete(FriendResult.PLAYER_DOES_NOT_EXIST);
        }
        getFriendRequests().remove(requestUUID);
        getFriends().add(requestUUID);
        mp.getFriends().add(this.uuid);
        EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
        completedFuture.complete(FriendResult.SUCCESS_ACCEPT);
      });
    } else {
      MallPlayer mp = MallPlayer.getPlayer(requestUUID);
      if (mp == null) {
        completedFuture.complete(FriendResult.PLAYER_DOES_NOT_EXIST);
      }
      getFriendRequests().remove(requestUUID);
      getFriends().add(requestUUID);
      mp.getFriends().add(this.uuid);
//            Messaging.sendMessage(p, "&6{0}{1} &6has just accepted your friend request!", this.getRank().getColor(), this.name);
      EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
      completedFuture.complete(FriendResult.SUCCESS_ACCEPT);
    }
    return completedFuture;
  }

  public CompletableFuture<FriendResult> denyFriendRequest(UUID requestUUID) {
    CompletableFuture<FriendResult> completedFuture = new CompletableFuture<>();
    if (requestUUID == this.uuid) {
      completedFuture.complete(FriendResult.FRIEND_SELF);
    }
    if (!getFriendRequests().contains(requestUUID)) {
      completedFuture.complete(FriendResult.NO_FRIEND_REQUEST);
    }
    if (getPlayer(requestUUID) == null) {
      PlayerData.getInstance().getPlayerByUUID(requestUUID).whenComplete((mp, throwable) -> {
        if (mp == null) {
          completedFuture.complete(FriendResult.PLAYER_DOES_NOT_EXIST);
        }
        getFriendRequests().remove(requestUUID);
        EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
        completedFuture.complete(FriendResult.SUCCESS_DENIED);
      });
    } else {
      MallPlayer mp = MallPlayer.getPlayer(requestUUID);
      if (mp == null) {
        completedFuture.complete(FriendResult.PLAYER_DOES_NOT_EXIST);
      }
      getFriendRequests().remove(requestUUID);
      EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
      completedFuture.complete(FriendResult.SUCCESS_DENIED);
    }
    return completedFuture;
  }

  public CompletableFuture<FriendResult> removeFriend(UUID friendUUID) {
    CompletableFuture<FriendResult> completedFuture = new CompletableFuture<>();
    if (friendUUID == this.uuid) {
      completedFuture.complete(FriendResult.FRIEND_SELF);
    }
    if (getPlayer(friendUUID) == null) {
      PlayerData.getInstance().getPlayerByUUID(friendUUID).whenComplete((mp, throwable) -> {
        if (mp == null) {
          completedFuture.complete(FriendResult.PLAYER_DOES_NOT_EXIST);
        }
        if (!mp.getFriends().contains(this.uuid)) {
          completedFuture.complete(FriendResult.NOT_FRIENDS);
        }
        if (!this.getFriends().contains(friendUUID)) {
          completedFuture.complete(FriendResult.NOT_FRIENDS);
        }
        if (mp.getFriendRequests().contains(this.uuid)) {
          completedFuture.complete(FriendResult.NOT_FRIENDS);
        }
        mp.getFriends().remove(this.uuid);
        this.getFriends().remove(friendUUID);
        EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
        completedFuture.complete(FriendResult.SUCCESS_REMOVED);
      });
    } else {
      MallPlayer mp = MallPlayer.getPlayer(friendUUID);
      if (mp == null) {
        completedFuture.complete(FriendResult.PLAYER_DOES_NOT_EXIST);
      }
      if (!mp.getFriends().contains(this.uuid)) {
        completedFuture.complete(FriendResult.NOT_FRIENDS);
      }
      if (!this.getFriends().contains(friendUUID)) {
        completedFuture.complete(FriendResult.NOT_FRIENDS);
      }
      if (mp.getFriendRequests().contains(this.uuid)) {
        completedFuture.complete(FriendResult.NOT_FRIENDS);
      }
      mp.getFriends().remove(this.uuid);
      this.getFriends().remove(friendUUID);
      EventHandler.triggerEvent(EventType.PLAYER_CHANGE, new PlayerChangeEvent(this));
      completedFuture.complete(FriendResult.SUCCESS_REMOVED);
    }
    return completedFuture;
  }

  public ChatChannel getChatChannel() {
    return chatChannel;
  }

  public void setChatChannel(ChatChannel channel) {
    this.chatChannel = channel;
  }

  public EnumLanguageIdentifier getLanguageIdentifier() {
    return languageIdentifier;
  }

  public EnumLanguageIdentifier setLanguageIdentifier(EnumLanguageIdentifier languageIdentifier) {
    return this.languageIdentifier = languageIdentifier;
  }

  public boolean isDebugMode() {
    return debugMode;
  }

  public void setDebugMode(boolean debugMode) {
    if(permissionSet.getPower() < PermissionSet.STAFF.getPower()){
      debugMode = false;
    }
    this.debugMode = debugMode;
  }

  public String getChatPrefix() {
    if (chatPrefix != null) {
      return chatPrefix + " ";
    } else {
      return "";
    }
  }

  public void setChatPrefix(String prefix) {
    this.chatPrefix = prefix;
  }

  public String getChatSuffix() {
    if (chatSuffix != null) {
      return chatSuffix + " ";
    } else {
      return "";
    }
  }

  public String getDiscordID() {
    return discordID;
  }

  public void setDiscordID(String discordID) {
    this.discordID = discordID;
  }

  public void setChatSuffix(String chatSuffix) {
    this.chatSuffix = chatSuffix;
  }

  public String getDisplayName() {
    return rank.getPrefix() + rank.getColor() + username;
  }
  public String getDisplayName(ChatColor color) {
    return rank.getPrefix() + color + username;
  }

  public enum FriendResult {
    SUCCESS("&6Successfully sent friend request"),
    SUCCESS_ACCEPT("&6Accepted friend request"),
    SUCCESS_DENIED("&6Denied friend request"),
    SUCCESS_REMOVED("&6Removed friend"),
    ALREADY_FRIENDS("&cYou are already friends with that player"),
    NOT_FRIENDS("&cYou aren't friends with that player"),
    NO_FRIEND_REQUEST("&cYou haven't got a friend request from that player"),
    PLAYER_DOES_NOT_EXIST("&cThat player does not exist"),
    FRIENDS_DISABLED("&cThat player has friend requests disabled."),
    ALREADY_FRIEND_REQUEST("&cYou have already sent a friend request!"),
    FRIEND_SELF("&cYou cannot be friends with yourself!");

    private String message;

    FriendResult(String message) {
      this.message = message;
    }

    public String getMessage() {
      return message;
    }
  }
}
