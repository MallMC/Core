package info.mallmc.core.api.player;

import com.google.gson.annotations.SerializedName;

public enum EnumLanguageIdentifier
{
    @SerializedName("EN")
    EN("English");

    private final String displayName;

    EnumLanguageIdentifier(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
