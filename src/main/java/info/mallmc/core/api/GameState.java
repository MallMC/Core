package info.mallmc.core.api;

import info.mallmc.core.MallCore;
import info.mallmc.core.database.ServerData;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.GameStateChangeEvent;

public enum GameState {

  LOBBY(0),
  SETUP(1),
  IN_GAME(2),
  ENDING(3),
  NOT_IN_GAME(4);

  private int power;

  GameState(int power) {
    this.power = power;
  }

  public int getPower() {
    return power;
  }

  public void setPower(int power) {
    this.power = power;
  }

  public static GameState getGameStateFromPower(int power) {
    GameState permissionSet = GameState.NOT_IN_GAME;
    for (GameState permSet : GameState.values()) {
      if (permSet.getPower() == power) {
        permissionSet = permSet;
      }
    }
    return permissionSet;
  }

  private static GameState gameState;

  public static GameState getGameState() {
    return gameState;
  }

  public static void setGameState(GameState state) {
    if(gameState != state) {
      gameState = state;
      EventHandler.handleEvent(EventType.GAMESTATE_CHANGE_EVENT, new GameStateChangeEvent());
      MallCore.getInstance().getServer().setGameState(gameState);
      ServerData.getInstance().saveServer(MallCore.getInstance().getServer());
    }
  }
}
