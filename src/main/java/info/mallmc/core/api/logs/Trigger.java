package info.mallmc.core.api.logs;

public enum Trigger {

  LOAD,
  COMMAND,
  COMMAND_MAP,
  PARTICLE,
  PLAYER,
  MESSAGE,
  SIGN,
  VISUAL
}
