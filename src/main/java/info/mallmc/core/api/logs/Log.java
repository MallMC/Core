package info.mallmc.core.api.logs;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import info.mallmc.core.MallCore;
import info.mallmc.core.util.TextUtils;
import info.mallmc.core.util.WebHookHandler;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;

public class Log {

  private static Datastore database = MallCore.getInstance().getDatabase().getDatastore();
  private static String lastError = "";

  public static void info(String message, Object... replacements) {
    info(LL.INFO, message, replacements);
  }

  public static void info(LL level, String message, Object... replacements) {
    message = TextUtils.replace(message, replacements);
    System.out.println("[" + level.getTag() + "] " + message);
  }

  /**
   * Log general actions into our database
   *
   * @param level Level of the action
   * @param player The player who did the action
   * @param action The action itself
   * @param details A few details about the action
   * @param replacements any replacements for the details
   */
  public static void action(LL level, String server, String player, String action, String details,
      Object... replacements) {
    DBObject obj = new BasicDBObject();
    obj.put("level", level.getTag());
    obj.put("server", server);
    obj.put("player", player);
    obj.put("action", action);
    obj.put("details", TextUtils.replace(details, replacements));
    obj.put("timestamp", System.currentTimeMillis());
    database.getDB().getCollection("logs_actions").insert(obj);
  }

  /**
   * Logs an error into our database as well as outputting it to the console
   *
   * @param level The level of the error
   * @param trigger The thing that triggered it
   * @param description The description of the error
   * @param error The error itself
   */
  public static void error(LL level, String server, String trigger, String description,
      String error) {
    if (description.equals(lastError)) {
      return;
    }
    Log.info(level, description);
    lastError = description;
    if (error == null) {
      error = description;
    }
    DBObject obj = new BasicDBObject();
    obj.put("level", level.getTag());
    obj.put("server", server);
    obj.put("error_trigger", trigger.toLowerCase());
    obj.put("description", description);
    obj.put("error", error);
    obj.put("solved", false);
    obj.put("timestamp", System.currentTimeMillis());
    database.getDB().getCollection("logs_errors").insert(obj);
    System.out.print("[Error] " + level  + " " + trigger + " " +error);
    WebHookHandler.sendDiscordAlert(level, description, trigger, server);
  }

  /**
   * Logs a broadcast into our database and into the console
   *
   * @param broadcaster The person who broadcasted the message
   * @param message The message that was broadcast
   */
  public static void broadcast(String broadcaster, String server, String message) {
    Log.info("{0} broadcasted {1}", broadcaster, message);
    DBObject obj = new BasicDBObject();
    obj.put("server", server);
    obj.put("broadcaster", broadcaster);
    obj.put("message", message);
    obj.put("timestamp", System.currentTimeMillis());
    database.getDB().getCollection("logs_broadcasts").insert(obj);
  }

  /**
   * Logs a kick into our database with the kicker, kicked player and the
   * reasoning
   *
   * @param kicker The person who kicked them
   * @param kickedPlayer The player who was kicked
   * @param reason The reason why the player was kicked
   */
  public static void kick(String kicker, String server, String kickedPlayer, String reason) {
    Log.info("{0} has just kicked {1} for {2}", kicker, kickedPlayer, reason);
    DBObject obj = new BasicDBObject();
    obj.put("server", server);
    obj.put("kicker", kicker);
    obj.put("kicked", kickedPlayer);
    obj.put("reason", reason);
    obj.put("timestamp", System.currentTimeMillis());
    database.getDB().getCollection("logs_kicks").insert(obj);
  }

  public static void sendErrorSlack(String ID) {
    DBObject query = new BasicDBObject("_id", new ObjectId(ID));
    DBObject obj = database.getDB().getCollection("logs_errors").findOne(query);
//        Bungee.getInstance().getSlackManager().alertToSlack(LL.valueOf((String)obj.get("level")),(String) obj.get("error"),(String) obj.get("error_trigger"),(String)obj.get("server"));
  }
}
