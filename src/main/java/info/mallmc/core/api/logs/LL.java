package info.mallmc.core.api.logs;

public enum LL {

  DEBUG("DEBUG"), // Does not show up on non-development servers
  INFO("INFO"),
  WARNING("WARN"),
  ERROR("ERORR"),
  CRITICAL("CRIT"); // If shit's hit the fan

  private String tag;

  LL(String tag) {
    this.tag = tag;
  }

  /**
   * Get a shortened version of the name, usually 4 letters
   *
   * @return The tag
   */
  public String getTag() {
    return tag;
  }

}
