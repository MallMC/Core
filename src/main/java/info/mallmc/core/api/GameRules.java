package info.mallmc.core.api;

public enum GameRules {
  ANNOUNCE_ADVANCEMENTS("announceAdvancements"),
  COMMAND_BLOCK_OUTPUT("commandBlockOutput"),
  DISABLE_ELYTRA_MOVEMENT_CHECK("disableElytraMovementCheck"),
  DO_DAYLIGHT_CYCLE("doDaylightCycle"),
  DO_ENTITY_DROPS("doEntityDrops"),
  DO_FIRE_TICK("doFireTick"),
  DO_LIMITED_CRAFTING("doLimitedCrafting"),
  DO_MOB_LOT("doMobLoot"),
  DO_MOB_SPAWNING("doMobSpawning"),
  DO_TILE_DROPS("doTileDrops"),
  DO_WEATHER_CYCLE("doWeatherCycle"),
  GAME_LOOP_FUNCTION("gameLoopFunction"),
  KEEP_INVENTORY("keepInventory"),
  LOG_ADMIN_COMMANDS("logAdminCommands"),
  MAX_COMMAND_CHAIN_LENGTH("maxCommandChainLength"),
  MAX_ENTITY_CRAMMING("maxEntityCramming"),
  MOB_GRIEVING("mobGriefing"),
  NATURAL_REGENERATION("naturalRegeneration"),
  RANDOM_TICK_SPEED("randomTickSpeed"),
  REDUCED_DEBUG_INFO("reducedDebugInfo"),
  SEND_COMMAND_FEEDBACK("sendCommandFeedback"),
  SHOW_DEATH_MESSAGES("showDeathMessages"),
  SPAWN_RADIUS("spawnRadius"),
  SPECTATORS_GENERATE_CHUNKS("spectatorsGenerateChunks");

  private final String name;

  GameRules(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
