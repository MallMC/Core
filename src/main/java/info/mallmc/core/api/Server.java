package info.mallmc.core.api;

import info.mallmc.core.api.player.Rank;
import java.util.ArrayList;
import java.util.UUID;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.PostLoad;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

@Entity
public class Server {

  @Id
  private ObjectId id;
  @Indexed(options = @IndexOptions(unique = true))
  private UUID uuid;
  private String nickname;
  private String ip;
  private int port;
  private boolean whitelisted;
  private ArrayList<UUID> override;
  @Transient
  private Rank whitelistLevel;
  @Property("whitelistLevel")
  private int whitelistLevelPower;
  @Transient
  private GameState gameState;
  @Transient
  private int xpModifer;
  @Property("gameState")
  private int gameStateID;
  private int playerCount;
  private long lastChange;

  public Server(String ipAddress, int port){
    this.uuid = UUID.randomUUID();
    this.ip = ipAddress;
    this.port = port;
    this.whitelisted = false;
    this.xpModifer = 1;
    this.override = new ArrayList();
    this.whitelistLevel = Rank.CONTRACTOR;
    this.gameState = GameState.NOT_IN_GAME;
    this.playerCount = 0;
    this.lastChange = System.currentTimeMillis();
  }


  public Server(){
    this.uuid = UUID.randomUUID();
    this.whitelisted = false;
    this.override = new ArrayList();
    this.xpModifer = 1;
    this.whitelistLevel = Rank.CONTRACTOR;
    this.gameState = GameState.NOT_IN_GAME;
    this.playerCount = 0;
    this.lastChange = System.currentTimeMillis();
  }

  @PrePersist
  void prePersist(){
    this.whitelistLevelPower = whitelistLevel.getId();
    this.gameStateID = gameState.getPower();
    this.lastChange = System.currentTimeMillis();
  }

  @PostLoad
  void postLoad(){
    this.whitelistLevel = Rank.getRankFromID(whitelistLevelPower);
    this.gameState = GameState.getGameStateFromPower(gameStateID);
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  public String getNickname() {
    if(nickname == null || nickname.isEmpty()){
      return ip +":" +port;
    }
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public boolean isWhitelisted() {
    return whitelisted;
  }

  public void setWhitelisted(boolean whitelisted) {
    this.whitelisted = whitelisted;
  }

  public ArrayList<UUID> getOverride() {
    return override;
  }

  public void setOverride(ArrayList<UUID> override) {
    this.override = override;
  }

  public Rank getWhitelistLevel() {
    return whitelistLevel;
  }

  public void setWhitelistLevel(Rank whitelistLevel) {
    this.whitelistLevel = whitelistLevel;
  }

  public GameState getGameState() {
    return gameState;
  }

  public void setGameState(GameState gameState) {
    this.gameState = gameState;
  }

  public int getPlayerCount() {
    return playerCount;
  }

  public void setPlayerCount(int playerCount) {
    this.playerCount = playerCount;
  }

  public long getLastChange() {
    return lastChange;
  }

  public void setLastChange(long lastChange) {
    this.lastChange = lastChange;
  }

  public int getXpModifer() {
    return xpModifer;
  }
}
