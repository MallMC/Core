package info.mallmc.core.api.language;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import info.mallmc.core.MallCore;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.util.WebHookHandler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.mallmc.core.api.player.EnumLanguageIdentifier;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Transient;

@Entity("languages")
public class Language {

  @Id
  private ObjectId id;
  @Indexed(options = @IndexOptions(unique = true))
  private EnumLanguageIdentifier identifier;
  private String name;
  private Map<String, Object> strings;
  @Transient
  private String languageJSON;

  public Language(){
    this.strings = new HashMap<>();
  }

  public Language(EnumLanguageIdentifier identifier, String name){
    this.identifier = identifier;
    this.name = name;
    this.strings = new HashMap<>();
  }

  public ObjectId getId() {
    return id;
  }

  public EnumLanguageIdentifier getIdentifier() {
    return identifier;
  }

  public String getName() {
    return name;
  }

  public List<String> getStrings(String key){
    if (languageJSON == null) {
      Gson gson = new Gson();
      languageJSON = gson.toJson(strings);
    }
    try {
      JsonArray jsonArray  = fromString(languageJSON, key).getAsJsonArray();
      List<String> list = new ArrayList<>();
      for (int i=0; i<jsonArray.size(); i++) {
        list.add(jsonArray.get(i).getAsString());
      }
      return list;
    } catch (NullPointerException e) {
      return Arrays.asList("Could not find message with key " + key);
    }
  }

  public String getMessage(String key) {
    if (languageJSON == null) {
      Gson gson = new Gson();
      languageJSON = gson.toJson(strings);
    }
    try {
      return fromString(languageJSON, key).getAsString();
    } catch (NullPointerException e) {
      WebHookHandler.sendDiscordAlert(LL.WARNING, "Unable to find message: " + key, "Messaging",
          MallCore.getInstance().getServer().getNickname());
      return "Could not find: " + key;
    }
  }
  public JsonElement fromString(String json, String path)
      throws JsonSyntaxException {
    JsonObject obj = new GsonBuilder().create().fromJson(json, JsonObject.class);
    String[] seg = path.split("\\.");
    for (String element : seg) {
      if (obj != null) {
        JsonElement ele = obj.get(element);
        if (!ele.isJsonObject())
          return ele;
        else
          obj = ele.getAsJsonObject();
      } else {
        return null;
      }
    }
    return obj;
  }

}
