package info.mallmc.core.database;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.bans.Ban;
import info.mallmc.core.api.bans.Offense;
import info.mallmc.core.util.BanUtil;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import org.mongodb.morphia.Datastore;

public class BanDB {

  private static BanDB banDB;

  public static BanDB getInstance() {
    if (banDB == null) {
      banDB = new BanDB();
    }
    return banDB;
  }

  private final Datastore datastore = MallCore.getInstance().getDatabase().getDatastore();

  public synchronized CompletableFuture<Boolean> hasBanExpired(UUID uuid) {
    CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
    PlayerData.getInstance().getPlayerByUUID(uuid).whenComplete(((player, throwable) -> {
      if (player == null) {
        completableFuture.complete(true);
        return;
      }
      if (!player.isBanned()) {
        completableFuture.complete(true);
        return;
      }
      long time = Long.parseLong(player.getBanExpiry());
      long now = System.currentTimeMillis() / 1000;
      if (time > 0 && now >= time) {
        completableFuture.complete(true);
        return;
      }
      completableFuture.complete(false);
    }));
    return completableFuture;
  }

  public synchronized CompletableFuture<Integer> getBanPoints(UUID player){
    CompletableFuture<Integer> completedFuture = new CompletableFuture<>();
    PlayerData.getInstance().getPlayerByUUID(player).whenComplete(((mallPlayer, throwable) -> {
      if (mallPlayer == null) {
        completedFuture.complete(0);
      }
      completedFuture.complete(mallPlayer.getBanPoints());
    }));
    return completedFuture;
  }

  public synchronized void banPlayer(UUID banner, UUID banned, Offense offense, String evidence, boolean permanent){
    if(!permanent){
      getBanPoints(banned).whenComplete((playerPoints, throwable) -> {
        int banPoints = playerPoints + offense.getPoints();
        String expiry = BanUtil.getBanEndTime(banPoints) + "";
        Ban ban = new Ban(banner, banned, offense.getPoints(), offense.getName(), evidence, "" + System.currentTimeMillis() / 1000, expiry);
        datastore.save(ban);
        PlayerData.getInstance().getPlayerByUUID(banned).whenComplete(((mallPlayer, throwable1) -> {
          mallPlayer.setBanExpiry(expiry);
          mallPlayer.setBanned(true);
          mallPlayer.setBanReason(offense.getName());
          mallPlayer.setBanPoints(banPoints);
          PlayerData.getInstance().savePlayer(mallPlayer);
        }));
      });
    }else{
      String expiry = BanUtil.getBanEndTime(10) + "";
      Ban ban = new Ban(banner, banned, offense.getPoints(), offense.getName(), evidence, "" + System.currentTimeMillis() / 1000, expiry);
      datastore.save(ban);
      PlayerData.getInstance().getPlayerByUUID(banned).whenComplete(((mallPlayer, throwable) -> {
        mallPlayer.setBanExpiry(expiry);
        mallPlayer.setBanned(true);
        mallPlayer.setBanReason(offense.getName());
        mallPlayer.setBanPoints(10);
        PlayerData.getInstance().savePlayer(mallPlayer);
      }));
    }
  }

  public synchronized void unBanPlayer(UUID uuidOfPlayerToBeUnbanned){
    PlayerData.getInstance().getPlayerByUUID(uuidOfPlayerToBeUnbanned).whenComplete((mallPlayer, throwable) -> {
      mallPlayer.setBanned(false);
      mallPlayer.setBanExpiry("");
      mallPlayer.setBanReason("");
      PlayerData.getInstance().savePlayer(mallPlayer);
    });
  }

  public synchronized List<Ban> getBansFromUUID(UUID playerUUID){
    final List<Ban> bans = datastore.createQuery(Ban.class).field("uuid")
        .equal(playerUUID).asList();
    return bans;
  }

  public synchronized Ban getActiveBanFromUUID(UUID playerUUID){
    final List<Ban> bans = datastore.createQuery(Ban.class).field("uuid")
        .equal(playerUUID).asList();
    Ban activeBan = null;
    for (int i = 0; i < bans.size(); i++) {
      Ban ban = bans.get(i);
      long time = Long.parseLong(ban.getEndDate());
      long now = System.currentTimeMillis() / 1000;
      if (time > 0 && now >= time) {
        continue;
      }
      activeBan = ban;
    }
    return activeBan;
  }
}

