package info.mallmc.core.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import info.mallmc.core.MallCore;
import info.mallmc.core.api.language.Language;
import java.util.Arrays;
import java.util.HashMap;

import info.mallmc.core.api.player.EnumLanguageIdentifier;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.LanguageLoadEvent;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

public class Database {

  private MongoClient client;
  private Morphia morphia;
  private Datastore datastore;

  public boolean connect(String ip, int port, String username, String password) {
    MongoCredential credential = MongoCredential
        .createScramSha1Credential(username, "admin", password.toCharArray());
    client = new MongoClient(new ServerAddress(ip, port), Arrays.asList(credential));
    morphia = new Morphia();
    return true;
  }

  public void setupMorphia(){
    morphia.mapPackage("info.mallmc.*.api");
    datastore = morphia.createDatastore(client, "mallmc");
    datastore.ensureIndexes();
  }

  public void setupLanguage(){
    HashMap<EnumLanguageIdentifier, Language> languageHashMap = LanguageData.getInstance().getLanguages();
    if(languageHashMap.size() == 0){
      for(EnumLanguageIdentifier languageIdentifier: EnumLanguageIdentifier.values()) {
        Language language = new Language(languageIdentifier, languageIdentifier.getDisplayName());
        languageHashMap.put(languageIdentifier, language);
        datastore.save(language);

      }
    }
    MallCore.getInstance().setLanguageMap(languageHashMap);
    EventHandler.handleEvent(EventType.LANG_RELOAD, new LanguageLoadEvent());
  }

  public void close() {
    this.client.close();
  }

  public Morphia getMorphia() {
    return morphia;
  }

  public Datastore getDatastore() {
    return datastore;
  }
}
