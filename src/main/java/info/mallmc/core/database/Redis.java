package info.mallmc.core.database;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import com.google.gson.*;
import info.mallmc.core.MallCore;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.core.events.EventType;
import java.util.ArrayList;
import java.util.List;
import redis.clients.jedis.Jedis;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

public class Redis {

  private Jedis jedis;
  private Jedis jedisSub;
  private Gson gson;

  public boolean connect(String host, String password, int port){
    gson = new GsonBuilder().registerTypeAdapter(Multimap.class,
        (JsonSerializer<Multimap>) (multimap, type, jsonSerializationContext) -> jsonSerializationContext
            .serialize(multimap.asMap()))
        .registerTypeAdapter(Multimap.class,
            (JsonDeserializer<Multimap>) (jsonElement, type, jsonDeserializationContext) -> {
              final SetMultimap<String, String> map = Multimaps
                  .newSetMultimap(new HashMap<String, Collection<String>>(),
                      Sets::newHashSet);
              for (Entry<String, JsonElement> entry : ((JsonObject) jsonElement).entrySet()) {
                for (JsonElement element : (JsonArray) entry.getValue()) {
                  map.get(entry.getKey())
                      .add(element.getAsString());
                }
              }
              return map;
            }).create();
    jedis = new Jedis(host, port);
    if(password == null || password.isEmpty()){
      return true;
    }
    jedis.auth(password);

    jedisSub = new Jedis(host, port);
    jedisSub.auth(password);
    List<String> channlesList = new ArrayList<>();
    for(EventType type: EventType.values()) {
      channlesList.add(type.getChannel());
    }
    String[] channels = channlesList.toArray(new String[0]);
    Runnable task = () -> jedisSub
        .subscribe(new RedisSub(), channels);

    restartThread(task);
    return true;
  }

  public void restartThread(Runnable runnable){
    Thread thread = new Thread(runnable);
    Thread.UncaughtExceptionHandler h = (th, ex) -> {
      Log.error(LL.CRITICAL, MallCore.getInstance().getServer().getNickname(), "Redis", "Redis uncaught error", ex.getMessage());
      restartThread(runnable);
    };
    thread.setUncaughtExceptionHandler(h);
    thread.start();
  }

  public boolean isOnline() {
    return jedis.isConnected();
  }

  public void stop() {
    jedis.close();
  }

  public boolean isInCache(String key) {
    return jedis.exists(key);
  }

  public void sendMessage(String channel, String message){
    jedis.publish(channel, message);
  }

  public Object getFromCache(String key, Class type) {
    if (jedis.exists(key)) {
      String obj = jedis.get(key);
      return gson.fromJson(obj, type);
    }
    return null;
  }

  public boolean putInCache(String key, Object value, Class type) {
    if (jedis.exists(key)) {
      return false;
    }
    String serialized = gson.toJson(value, type);
    jedis.set(key, serialized);
    jedis.expire(key, 60 * 60);
    if (jedis.exists(key)) {
      return true;
    }
    return false;
  }

  public Gson getGson() {
    return gson;
  }

}
