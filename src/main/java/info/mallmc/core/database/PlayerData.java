package info.mallmc.core.database;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.Rank;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import org.mongodb.morphia.Datastore;

public class PlayerData {

  private static PlayerData playerData;

  public static PlayerData getInstance() {
    if (playerData == null) {
      playerData = new PlayerData();
    }
    return playerData;
  }

  private final Datastore datastore = MallCore.getInstance().getDatabase().getDatastore();

  /**
   * Does the player exist in the database?
   *
   * @param uuid The UUID of the player to find
   * @return Whether or not the player exists in the database
   */
  public synchronized CompletableFuture<Boolean> isInDatabase(UUID uuid) {
    CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
    MallCore.getInstance().getMallCoreSchedulerService().execute(() -> {
      final List<MallPlayer> players = datastore.createQuery(MallPlayer.class).field("uuid")
          .equal(uuid).asList();
      if (players.size() > 0 && players.size() == 1) {
        completableFuture.complete(true);
      } else {
        completableFuture.complete(false);
      }
    });
    return completableFuture;
  }

  /**
   * Get a player by their UUID
   *
   * @param username The Username of the player to get
   * @return The actual player in the database
   */
  public synchronized CompletableFuture<MallPlayer> getPlayerByUsername(String username) {
    CompletableFuture<MallPlayer> completableFuture = new CompletableFuture<>();
   MallCore.getInstance().getMallCoreSchedulerService().execute(() -> {
      final List<MallPlayer> players = datastore.createQuery(MallPlayer.class).field("username")
          .equalIgnoreCase(username).asList();
      if (players.size() > 0 && players.size() == 1) {
        completableFuture.complete(players.get(0));
      } else {
        completableFuture.complete(null);
      }
    });
    return completableFuture;
  }

  /**
   * Get a player by their UUID
   *
   * @param uuid The UUID of the player to get
   * @return The actual player in the database
   */
  public synchronized CompletableFuture<MallPlayer> getPlayerByUUID(UUID uuid) {
    CompletableFuture<MallPlayer> completableFuture = new CompletableFuture<>();
    MallCore.getInstance().getMallCoreSchedulerService().execute(() -> {
      final List<MallPlayer> players = datastore.createQuery(MallPlayer.class).field("uuid")
          .equal(uuid).asList();
      if (players.size() > 0 && players.size() == 1) {
        completableFuture.complete(players.get(0));
      } else {
        completableFuture.complete(null);
      }
    });
    return completableFuture;
  }

  /**
   * Get the UUID of a player by their username
   * @param username  The username of the player to find
   * @return          The uuid of the user
   */
  public synchronized CompletableFuture<UUID> getUUIDByUsername(String username){
    CompletableFuture<UUID> completableFuture = new CompletableFuture<>();
    MallCore.getInstance().getMallCoreSchedulerService().execute(() -> {
      final List<MallPlayer> players = datastore.createQuery(MallPlayer.class).field("username")
          .equalIgnoreCase(username).asList();
      if (players.size() > 0 && players.size() == 1) {
        completableFuture.complete(players.get(0).getUuid());
      } else {
       completableFuture.complete(null);
      }
    });
    return completableFuture;
  }

  /**
   * Gets the permission set from the DB by the players UUID
   *
   * @param uuid The uuid of the player to get
   * @return The rank of the player from the database
   */
  public synchronized CompletableFuture<Rank> getRankFromDatabase(UUID uuid) {
    CompletableFuture<Rank> completableFuture = new CompletableFuture<>();
    MallCore.getInstance().getMallCoreSchedulerService().execute(() -> {
      Rank permSet = Rank.DEFAULT;
      final List<MallPlayer> players = datastore.createQuery(MallPlayer.class).field("uuid").equal(uuid).asList();
      if (players != null) {
        permSet = players.get(0).getRank();
      }
      completableFuture.complete(permSet);
    });
    return completableFuture;
  }

  /**
   * Save the player to the database
   *
   * @param mallPlayer The player to save to the database
   */
  public synchronized CompletableFuture<Boolean> savePlayer(MallPlayer mallPlayer) {
    CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
    MallCore.getInstance().getMallCoreSchedulerService().execute(() -> {
      final List<MallPlayer> players = datastore.createQuery(MallPlayer.class).field("uuid").equal(mallPlayer.getUuid()).asList();
      if (players == null || players.size() < 1) {
        datastore.save(mallPlayer);
        completableFuture.complete(true);
        return;
      }
      MallPlayer savedPlayer = players.get(0);
      mallPlayer.setId(savedPlayer.getId());
      datastore.save(mallPlayer);
      completableFuture.complete(true);
    });
    return completableFuture;
  }

}
