package info.mallmc.core.database;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.Server;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

public class ServerData {

  private static ServerData serverData;

  public static ServerData getInstance() {
    if (serverData == null) {
      serverData = new ServerData();
    }
    return serverData;
  }

  private final Datastore datastore = MallCore.getInstance().getDatabase().getDatastore();

  public Server getCurrentServer(String ipAddress, int port){
    Query<Server> serverQuery = datastore.createQuery(Server.class);
    final List<Server> players = serverQuery.field("ip").equal(ipAddress).field("port").equal(port).asList();
    if (players.size() > 0 && players.size() == 1) {
      return players.get(0);
    } else {
      return null;
    }
  }

  /**
   * Save the server to the database
   *
   * @param server The player to save to the database
   */
  public synchronized void saveServer(Server server) {
    final List<Server> servers = datastore.createQuery(Server.class).field("ip").equal(server.getIp()).field("port").equal(server.getPort()).asList();
    if (servers == null || servers.size() < 1) {
      datastore.save(server);
      return;
    }
    Server savedServer = servers.get(0);
    server.setId(savedServer.getId());
    datastore.save(server);
  }

}
