package info.mallmc.core.database;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.Server;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.events.Event;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.AddServerEvent;
import info.mallmc.core.events.events.GameEndEvent;
import info.mallmc.core.events.events.GameStartEvent;
import info.mallmc.core.events.events.JoinGameEvent;
import info.mallmc.core.events.events.PlayerChangeEvent;
import info.mallmc.core.events.events.RemoveServerEvent;
import info.mallmc.core.events.events.ServerReadyEvent;
import info.mallmc.core.events.events.discord.DiscordMessageReceivedEvent;
import info.mallmc.core.events.events.discord.SendMessageToDiscordEvent;
import java.util.UUID;
import redis.clients.jedis.JedisPubSub;

public class RedisSub extends JedisPubSub {

  @Override
  public void onMessage(String channel, String message) {
    EventType eventType = null;
    for(EventType type: EventType.values()) {
      if (type.getChannel().equals(channel)) {
        eventType = EventType.getTypeFormString(channel);
      }
    }
    switch (eventType)
    {
      case JOIN_GAME_EVENT:
        if(MallCore.getInstance().getServer().getNickname().contains("bungee")){
          String[] data = message.split(":");
          String uuid = data[0];
          String gameName = data[1];
          UUID playerUUID = UUID.fromString(uuid);
          MallPlayer playerToTeleport = MallPlayer.getPlayer(playerUUID);
          if(playerToTeleport == null){
            return;
          }
          EventHandler.handleEvent(EventType.JOIN_GAME_EVENT, new JoinGameEvent(playerToTeleport, gameName));
        }
        break;
      case ADD_SERVER_EVENT:
        if(MallCore.getInstance().getServer().getNickname().contains("bungee")){
          String[] data = message.split(":");
          String serverUUID = data[0];
          String serverIP = data[1];
          int serverPort = Integer.parseInt(data[2]);
          String serverGame = data[3];
          Server server = new Server(serverIP, serverPort);
          server.setNickname(serverGame + "-" + serverUUID);
          ServerData.getInstance().saveServer(server);
          EventHandler.handleEvent(EventType.ADD_SERVER_EVENT, new AddServerEvent(serverIP, serverPort));
        }
        break;

      case SERVER_READY_EVENT:
        if(MallCore.getInstance().getServer().getNickname().contains("bungee")){
          String[] data = message.split(":");
          String serverIP = data[0];
          int serverPort = Integer.parseInt(data[1]);
          Server server = ServerData.getInstance().getCurrentServer(serverIP, serverPort);
          String gameName = server.getNickname().split("-")[0];
          EventHandler.handleEvent(EventType.SERVER_READY_EVENT, new ServerReadyEvent(server, gameName));
        }
        break;
      case GAME_END_EVENT:
        if(MallCore.getInstance().getServer().getNickname().contains("bungee")){
          String[] data = message.split(":");
          String serverIP = data[0];
          int serverPort = Integer.parseInt(data[1]);
          Server server = ServerData.getInstance().getCurrentServer(serverIP, serverPort);
          EventHandler.handleEvent(EventType.GAME_END_EVENT, new GameEndEvent(server));
        }
        break;
      case GAME_START_EVENT:
        if(MallCore.getInstance().getServer().getNickname().contains("bungee")){
          String[] data = message.split(":");
          String serverIP = data[0];
          int serverPort = Integer.parseInt(data[1]);
          String gameName = data[2];
          Server server = ServerData.getInstance().getCurrentServer(serverIP, serverPort);
          EventHandler.handleEvent(EventType.GAME_START_EVENT, new GameStartEvent(server, gameName));
        }
        break;
      case REMOVE_SERVER_EVENT:
        if(MallCore.getInstance().getServer().getNickname().contains("bungee")){
          EventHandler.handleEvent(EventType.REMOVE_SERVER_EVENT, new RemoveServerEvent(message));
        }
        break;

      case PLAYER_CURRENCY_CHANGE:
      case PLAYER_RANK_CHANGE:
      case PLAUER_LEVEUP_EVENT:
      case PLAYER_CHANGE:
      case PLAYER_XP_CHANGE:
          if (eventType.isPlayerChange()) {
            if(!MallCore.getInstance().getDatabase().getMorphia().isMapped(MallPlayer.class)){
              return;
            }
            PlayerChangeEvent changeEvent = MallCore.getInstance().getRedis().getGson().fromJson(message, PlayerChangeEvent.class);
            UUID uuid = changeEvent.getUpdatedPlayer().getUuid();
            String name = changeEvent.getUpdatedPlayer().getUsername();
            if(MallPlayer.getPlayers().containsKey(uuid)) {
              PlayerData.getInstance().savePlayer(changeEvent.getUpdatedPlayer()).whenComplete((hasSaved, throwable) -> {
                MallPlayer.removePlayer(uuid);
                MallPlayer.createPlayer(uuid, name);
              });
            }
          }
          break;
      case MESSAGE_TO_DISCORD_EVENT:
        EventHandler.handleEvent(eventType, MallCore.getInstance().getRedis().getGson().fromJson(message, SendMessageToDiscordEvent.class));
        break;
      case MESSAGE_FORM_DISCORD_EVENT:
        EventHandler.handleEvent(eventType, MallCore.getInstance().getRedis().getGson().fromJson(message, DiscordMessageReceivedEvent.class));
        break;
    }

    }

  @Override
  public void onPMessage(String pattern, String channel, String message) {
    super.onPMessage(pattern, channel, message);
  }

  @Override
  public void onSubscribe(String channel, int subscribedChannels) {
    super.onSubscribe(channel, subscribedChannels);
  }

  @Override
  public void onUnsubscribe(String channel, int subscribedChannels) {
    super.onUnsubscribe(channel, subscribedChannels);
  }

  @Override
  public void onPUnsubscribe(String pattern, int subscribedChannels) {
    super.onPUnsubscribe(pattern, subscribedChannels);
  }

  @Override
  public void onPSubscribe(String pattern, int subscribedChannels) {
    super.onPSubscribe(pattern, subscribedChannels);
  }
}
