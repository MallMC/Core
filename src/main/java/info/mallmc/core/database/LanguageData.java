package info.mallmc.core.database;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.language.Language;
import java.util.HashMap;
import java.util.List;

import info.mallmc.core.api.player.EnumLanguageIdentifier;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

public class LanguageData {
  private static LanguageData languageData;

  public static LanguageData getInstance() {
    if (languageData == null) {
      languageData = new LanguageData();
    }
    return languageData;
  }

  private final Datastore datastore = MallCore.getInstance().getDatabase().getDatastore();

  public synchronized HashMap<EnumLanguageIdentifier, Language> getLanguages(){
    Query<Language> languageQuery = datastore.createQuery(Language.class);
    final List<Language> languages = languageQuery.asList();
    HashMap<EnumLanguageIdentifier, Language> languageHashMap = new HashMap<>();
    for (Language language : languages){
      languageHashMap.put(language.getIdentifier(), language);
    }
    return languageHashMap;
  }
}
